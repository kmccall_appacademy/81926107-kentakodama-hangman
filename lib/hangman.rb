# We'll start out with the computer choosing the secret word and the human trying to guess it. The computer player should read in a dictionary file (lib/dictionary.txt) and choose a word randomly. There are specs to guide you through most of the project, then you'll have to go a little further to make it actually playable. Interaction should look something like so:
#
# Secret word: _____ > h Secret word: h____ > l Secret word: h_ll_ > z Secret word: h_ll_


class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players) #this is an options hash
    @guesser = players[:guesser] #options hash
    @referee = players[:referee]
    @board = []
  end

  def setup
    @secret_word_length = @referee.pick_secret_word
    @board = Array.new(@secret_word_length, '_')
    @guesser.register_secret_length(@secret_word_length)
    @secret_word_length
    #returns the letter count of the secret word
  end

  def take_turn
    #asks #guesser for a guesser
    letter = @guesser.guess
    matched_pos = @referee.check_guess(letter)
    update_board(letter, matched_pos)
    @guesser.handle_response(letter, matched_pos)
  end

  def update_board(letter, positions)
    @board.map.with_index do |char, i|
      if positions.include?(i)
        char = letter
      else
        char
      end
    end
  end

end

class HumanPlayer

  attr_reader :secret_length

  def register_secret_length(length)
    p "The word if #{length} letters long"
  end

  def guess
    p 'What letter?'
    guess = gets.chomp
    guess
  end

  def handle_response(guess, response)
    p "Found #{guess} at #{response}"
  end

end

class ComputerPlayer

  attr_reader :candidate_words, :dictionary, :secret_word

  #why does this work???????
  def self.player_with_dict_file(dict_file_name)
    ComputerPlayer.new(File.readlines(dict_file_name).map(&:chomp))
  end

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
    @guesses_so_far = []
  end

  def register_secret_length(length)
    @secret_word_length = length
    @candidate_words.select! {|word| word.length == @secret_word_length}
  end


  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def guess(board)

    frequencies_of_remain = freq_table(board)

    frequencies_of_remain.max_by{|k, v| v}[0]
    # if @guesses_so_far.empty?
    #   return 'e'
    # else
    #
    #   letters = @candidate_words.join('').split('').reject{|char| @guesses_so_far.include?(char)}
    #   most_freq = letters.uniq.reduce do |most, letter|
    #     if @candidate_words.count(most) < @candidate_words.count(letter)
    #       most = letter
    #     else
    #       most
    #     end
    #   end
    #
    #   return most_freq
    # end

  end

  def handle_response(guess, response)
    @guesses_so_far << guess
    #this rejects words without the guessed letter
    #@candidate_words.select! {|word| word.include?(guess)}

    @candidate_words.reject! {|word| word.index(guess) == -1}

    # get indices of guessed letter in an array
    @candidate_words.reject! do |word|
      indices = []
      word.split('').each_with_index do |char, i|
        indices << i if char == guess
      end
      indices != response
    end
    # compare and reject those that dont match the response


  end

  def check_guess(letter)
    results = []
    @secret_word.split('').each_index do |i|
      results << i if   @secret_word[i] == letter
    end
    results
  end

  def freq_table(board)
    freq = Hash.new(0)

    @candidate_words.join('').each_char do |char|
      freq[char] += 1 unless board.include?(char)
    end

    freq

  end





end
#
#
# example = ComputerPlayer.new('lib/dictionary.txt')#must have the path
#
# p example.dictionary
